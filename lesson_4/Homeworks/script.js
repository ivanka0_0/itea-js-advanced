        let form = document.forms.my_form;
        let myArrayCollection = Array.from(document.forms.my_form.elements);
        let errors = {
            my_name: "Как тебя зовут дружище?!",
            my_mail: "Ну и зря, не получишь бандероль с яблоками!",
            my_pass: "Я никому не скажу наш секрет",
            my_apple: "Ну хоть покушай немного... Яблочки вкусные",
            say_thanks: "Фу, неблагодарный(-ая)!",
            say_yes: "Необразованные живут дольше! Хорошо подумай!"
        };
        var check_valid = document.getElementById('check_valid');
        check_valid.onclick = function() {
            myArrayCollection.forEach(function(item) {
                Check_valid(item);
            });
            form.reportValidity();
            myArrayCollection.forEach(function(item) {
                item.oninput = function() {
                    item.setCustomValidity("");
                };
                item.onblur = function() {
                    myArrayCollection.forEach(function(item) {
                        Check_valid(item);
                    });
                    form.reportValidity();
                };
            });
        };

        var Check_valid = function(item) {
            switch (item) {
                case form.my_name:
                    {
                        if (item.validity.valueMissing) {
                            item.setCustomValidity(errors[item.name]);
                        }
                        break;
                    }
                case form.my_pass:
                case form.my_mail:
                case form.say_yes:
                    {
                        if (!item.validity.valid) {
                            item.setCustomValidity(errors[item.name]);
                        }
                        break;
                    }
                case form.my_apple:
                    {
                        if (item.value <= 0) {
                            item.setCustomValidity(errors[item.name]);
                        }
                        break;
                    }
                case form.say_thanks:
                    {
                        if (item.value !== "спасибо") {
                            item.setCustomValidity(errors[item.name]);
                        }
                        break;
                    }
                default:
                    return false;
            }
        };