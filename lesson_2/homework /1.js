/*

  Задание 1.

  Написать скрипт который будет будет переключать вкладки по нажатию
  на кнопки в хедере.

  Главное условие - изменять файл HTML нельзя.

  Алгоритм:
    1. Выбрать каждую кнопку в шапке
       + бонус выбрать одним селектором

    2. Повесить кнопку онклик
        button1.onclick = function(event) {

        }
        + бонус: один обработчик на все три кнопки

    3. Написать функцию которая выбирает соответствующую вкладку
       и добавляет к ней класс active

    4. Написать функцию hideAllTabs которая прячет все вкладки.
       Удаляя класс active со всех вкладок

  Методы для работы:

    getElementById
    querySelector
    classList
    classList.add
    forEach
    onclick

    element.onclick = function(event) {
      // do stuff ...
    }
*/
var showButton = document.getElementsByClassName('showButton');
var Buttons = Array.from(showButton);
Buttons.forEach(function(e) {
    e.onclick = function(event) {
        var target = event.target;
        tab = target.getAttribute('data-tab');
        var tabs = document.querySelector('div[data-tab = \"' + tab + '\"]');
        active_div = document.querySelector('div .active');
        hideTab();
        if (tab != null)
            tabs.classList.add('active');
    }
});

var hideTab = function() {
    if (active_div != null) {
        if (active_div.classList.contains("active") == true && tab != null)
            active_div.classList.remove("active");
    }
}